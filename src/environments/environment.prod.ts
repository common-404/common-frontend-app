const HOST = 'http://localhost:9999'
export const environment = {
  production: true,

  ENDPOINTS: {
    USER_CREATION: `${HOST}/user/create`,
    USER_LOGIN: `${HOST}/user/login`,
    USER_INFO: `${HOST}/user/info`,
  }
};
