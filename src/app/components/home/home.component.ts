import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";
import {User} from "../../models/user";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  loggedIn = false;
  userModel: User | undefined;

  constructor(private autheticationService: AuthenticationService,
              private router: Router) {

    this.autheticationService.currentUser$.subscribe(
      userModel => {
        this.loggedIn = userModel != null;
        this.userModel = userModel;
      }
    );
  }


  ngOnInit(): void {
  }

}
