import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loggedIn = false;

  constructor(private autheticationService: AuthenticationService,
              private router: Router) {

    this.autheticationService.currentUser$.subscribe(
      userModel => this.loggedIn = userModel != null
    );
  }


  ngOnInit(): void {
  }

  logoutClick(event: any) {
    event.preventDefault();

    this.autheticationService.logout();
    this.router.navigate(['/home']);
  }


}
