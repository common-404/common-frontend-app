import { environment } from './../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { User } from './../models/user';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {


  static USER_INFO = 'USER_INFO';

  private userInfoSubject: BehaviorSubject<User>;
  public currentUser$: Observable<User>;

  constructor(private httpClient: HttpClient,
              private cookieService: CookieService) {

    const jsonString = this.cookieService.get(AuthenticationService.USER_INFO);
    if (jsonString === '') {
      // @ts-ignore
      this.userInfoSubject = new BehaviorSubject<User>(null);
    } else {
      this.userInfoSubject = new BehaviorSubject<User>(JSON.parse(jsonString));
    }

    this.currentUser$ = this.userInfoSubject.asObservable();
  }


  login(email: string, password: string): Observable<User> {

    const url = `${environment.ENDPOINTS.USER_LOGIN}`;

    return this.httpClient.post<User>(url, { email, password })
      .pipe(
        tap({next: (data) => console.log(data),
          error: (error) => error.error = "Invalid Login"}),
        map(userModel => {

          // create a cookie with jwtToken
          this.cookieService.set(AuthenticationService.USER_INFO, JSON.stringify(userModel));

          // notify everybody else
          this.userInfoSubject.next(userModel);

          return userModel;
        })
      );
  }

  logout() {
    this.cookieService.delete(AuthenticationService.USER_INFO);

    // @ts-ignore
    this.userInfoSubject.next(null);
  }

  registerUser(formValue: any): Observable<User> {

    const url = `${environment.ENDPOINTS.USER_CREATION}`;

    return this.httpClient.post<User>(
      url,
      formValue
    ).pipe(
      map(userModel => {
        // create a cookie with jwtToken
        this.cookieService.set(AuthenticationService.USER_INFO, JSON.stringify(userModel));

        // notify everybody else
        this.userInfoSubject.next(userModel);

        return userModel;
      })
    );

  }

  fetchUserInfo(userId: string): Observable<User> {
    const url = `${environment.ENDPOINTS.USER_INFO}/${userId}`;
    return this.httpClient.get<User>(url);
  }


  public get currentUserValue() {
    return this.userInfoSubject.value;
  }
}
