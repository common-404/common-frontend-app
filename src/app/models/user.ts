export interface User {
  id: string;
  name: string;
  password: string;
  email: string;
  roles: string[];
  jwtToken: string;
  createdAt: string;
  updatedAt: string;

}
